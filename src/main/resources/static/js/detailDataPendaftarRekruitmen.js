let url;
if($(location).attr('href').includes("localhost")) {
    url = "http://localhost:8080";
} else {
    url = "http://api-rekruit.herokuapp.com";
}

let redirectUrl;
if($(location).attr('href').includes("localhost")) {
    redirectUrl = "http://localhost:8081";
} else {
    redirectUrl = "http://rekruit-b15.herokuapp.com";
}

$(document).ready(() => {
    const tokenRekruiter = window.localStorage.getItem('token');
    const idMendaftar = idPendaftar + "-" + idRekruitmen

    $.ajax({
        type: "GET",
        url: url + "/rekruter/mendaftar/" + idPendaftar + "/" + idRekruitmen,
        dataType: 'json',
        contentType: 'application/json',
        headers: {
                  "Access-Control-Allow-Origin": "*",
                  "Authorization": "Bearer " + tokenRekruiter,
                },
        success: (data) => {
            let checkNullCV = data.linkCV != null ? data.linkCV : ""
            let checkNullTugas = data.linkTugas != null ? data.linkTugas : ""
            let urlCV = checkNullCV.includes("http") ? data.linkCV : "http://" + data.linkCV;
            let urlTugas = checkNullTugas.includes("http") ? data.linkTugas : "http://" + data.linkTugas;

            $("#detailDataPendaftarRekruitmen").empty();
            let htmlCV = checkNullCV != "" ? '<p class="is-size-5"> <b>Curriculum Vitae: </b> <a href="'+
                urlCV +'">' + data.linkCV +'</a></p>' : '<p class="is-size-5"> <b>Curriculum Vitae: </b>' +
                "Belum Mengumpulkan" +'</p>';
            let htmlLinkTugas = checkNullTugas != "" ? '<p class="is-size-5"> <b>Tugas Khusus: </b><a href="'+
                urlTugas +'">' + data.linkTugas +'</a></p>' : '<p class="is-size-5"> <b>Tugas Khusus: </b>' +
                "Belum Mengumpulkan" +'</p>';
            let content = '<p class="is-size-5"> Mendaftar '+ data.rekruitmenModel.judul +'</p >' +
            '<br><br>' +
            '<p class="is-size-3" ><b>'+ data.pendaftarModel.namaLengkap +'</b></p >' +
            '<br>' +
            '<div class="card">' +
                '<div class="card-content">'+
                    '<div class="content">' +
                        '<p class="is-size-4" > <b>Profil Pendaftar</b></p >' +
                        '<p class="is-size-5"> <b>NPM: </b>' + data.pendaftarModel.npm +'</p>' +
                        '<p class="is-size-5"> <b>Fakultas: </b>' + data.pendaftarModel.fakultas +'</p>' +
                    '</div>' +
                '</div>' +
              '</div>'+
              '<br><br>' +
              '<div class="card">' +
                 '<div class="card-content">'+
                     '<div class="content">' +
                          '<p class="is-size-4"> <b>Submisi Tugas</b></p >' +
                          htmlCV +
                          htmlLinkTugas +
                     '</div>' +
                 '</div>' +
               '</div>' +
               '<br><br>' +
              '<div class="card">' +
                 '<div class="card-content">'+
                     '<div class="content">' +
                          '<p class="is-size-4"> <b>Penilaian</b></p >' +
                          '<p class="is-size-5"> <b>Nilai:</b> <input type="number" id="nilai" name="nilai" maxlength="3" size="3" value="'+data.nilai+'"> / 100</p>' +
                          '<p class="is-size-5"> <b>Status:</b> <select id="status" name="status">' +
                                                                 '<option value="Diterima">Diterima</option>' +
                                                                 '<option value="Ditolak">Ditolak</option>' +
                                                               '</select></p>' +
                           '<button class="button is-primary buttonSubmit">Perbarui</button>'+
                     '</div>' +
                 '</div>' +
               '</div>' +
               '<br><br>' +
               '<button class="button is-light buttonBack">Kembali</button>'
            $("#detailDataPendaftarRekruitmen").append(content);
        }
    });

});

$(document).on('click',".buttonSubmit", () => {
    let nilai = $("#nilai").val()
    let status = $("#status").val()
    const tokenRekruiter = window.localStorage.getItem('token');
    const idMendaftar = idPendaftar + "-" + idRekruitmen

    if($("#nilai").val() != "" && $("#status").val() != "" ) {
      $.ajax({
          type: "POST",
          url: url + "/rekruter/" +"menilai/" + idMendaftar +"/"+status+"/"+nilai,
          dataType: 'json',
          contentType: 'application/json',
          headers: {
            "Access-Control-Allow-Origin": "*",
            "Authorization": "Bearer " + tokenRekruiter,
          },
          success: () => {
              alert("Berhasil di Submit");
              window.location = redirectUrl + "/rekruitmen/detail-rekruitmen/"+ idRekruitmen;
          },
      });
    } else {
      alert("Data kosong");
    }
})

$(document).on('click',".buttonBack", () => {
    window.location = redirectUrl + "/rekruitmen/detail-rekruitmen/"+ idRekruitmen;
})

