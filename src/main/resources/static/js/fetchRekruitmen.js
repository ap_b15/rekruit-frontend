let url;
if($(location).attr('href').includes("localhost")) {
    url = "http://localhost:8080";
} else {
    url = "http://api-rekruit.herokuapp.com";
}
let urlFe;
if($(location).attr('href').includes("localhost")) {
    urlFe = "http://localhost:8081";
} else {
    urlFe = "http://rekruit-b15.herokuapp.com/";
}

let cachedRekruitmen;
let tokenRekruiter = window.localStorage.getItem('token');
let id = window.localStorage.getItem('id');

$(document).ready(() => {
    $.ajax({
      type: "GET",
      url: url + '/rekruter/' + id,
      dataType: 'json',
      contentType: 'application/json',
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Authorization": "Bearer " + tokenRekruiter,
      },
      success: (data) => {
        $('#rekruitmen-box').empty();
        cachedRekruitmen = data;
        showAllDash(cachedRekruitmen);

      }
    });
});

$("#show-all").click(() => {
    $("#rekruitmen-box").empty();
    showAllDash(cachedRekruitmen);
})

$("#show-belum-dibuka").click(() => {
    $("#rekruitmen-box").empty();
    showBelumDibukaDash(cachedRekruitmen);
})

$("#show-dibuka").click(() => {
    $("#rekruitmen-box").empty();
    showSudahDibukaDash(cachedRekruitmen);
})

$("#show-ditutup").click(() => {
    $("#rekruitmen-box").empty();
    showSudahDitutupDash(cachedRekruitmen);
})

function showAllDash(response) {
    response.forEach((element) => {
        renderElement(element);
    })
}

function showBelumDibukaDash(response) {
    response.forEach((element) => {
        if(element.status === 'Belum Dibuka') {
            renderElement(element);
        }
    })
}

function showSudahDibukaDash(response) {
    response.forEach((element) => {
        if(element.status === 'Sudah Dibuka') {
            renderElement(element);
        }
    })
}

function showSudahDitutupDash(response) {
    response.forEach((element) => {
        if(element.status === 'Sudah Ditutup') {
            renderElement(element);
        }
    })
}

function renderElement(element) {
    let rekruitmenId = element.id;
    let judul = element.judul;
    let dueDateRegis = new Date(element.dueDateRegistrasi).toDateString();
    let dueDateTugas = new Date(element.dueDateTugas).toDateString();
    let narahubung = element.narahubung;
    let status = element.status;
    let urlUbah = urlFe + '/rekruitmen/ubah/' + rekruitmenId;
    let urlBuatPengumuman = urlFe + '/pengumuman/buat/' + rekruitmenId;
    let urlLihatPengumuman = urlFe + '/pengumuman/' + rekruitmenId;
    let urlListPendaftar = url + '/rekruter/mendaftar/' + rekruitmenId;
    let urlDetil = urlFe + '/rekruitmen/detail-rekruitmen/' + rekruitmenId
    let jumlahPendaftar = 0;

    $.ajax({
        type: "GET",
        url: urlListPendaftar,
        dataType: 'json',
        contentType: 'application/json',
        async: false,
        headers: {
            "Access-Control-Allow-Origin": "*",
            "Authorization": "Bearer " + tokenRekruiter,
        },
        success: (data) => {
            jumlahPendaftar = data.length;
        },
        error: () => {
            jumlahPendaftar = -1;
        }
    });

    let rekruitmenCard = [
        '<div>',
            '<div class="card">',
                '<div class="card-content">',
                    `<p class="title is-5">${judul}</p>`,
                    `<p class="is-size-6">Due Date Registrasi: <strong>${dueDateRegis}</strong></p>`,
                    `<p class="is-size-6">Due Date Pengumpulan Tugas: <strong>${dueDateTugas}</strong></p>`,
                    `<p class="is-size-6">Status: <strong>${status}</strong></p>`,
                    `<p class="is-size-6">Narahubung: <strong>${narahubung}</strong></p>`,
                    `<p class="is-size-6">Jumlah Pendaftar: <strong>${jumlahPendaftar} orang</strong></p>`,
                    '<br>',
                    '<nav class="level">',
                        '<div class="level-left">',
                            '<div class="level-item">',
                                `<button class="button is-primary is-outlined buttonUpdate" id="${rekruitmenId}">`,
                                    `<a href="${urlUbah}">Update</a>`,
                                '</button>',
                            '</div>',
                            '<div class="level-item">',
                                '<button id="detil-pendaftar" class="button is-primary is-outlined">',
                                    `<a href="${urlDetil}">Lebih Lengkap</a>`,
                                '</button>',
                            '</div>',
                        '</div>',
                        '<div class="level-right">',
                            '<div class="level-item">',
                                '<button id="lihat-pengumuman" class="button is-primary is-outlined">',
                                    `<a href="${urlLihatPengumuman}">Lihat seluruh Pengumuman</a>`,
                                '</button>',
                            '</div>',
                            '<div class="level-item">',
                                '<button id="buat-pengumuman" class="button is-primary">',
                                    `<a href="${urlBuatPengumuman}">Buat Pengumuman Baru</a>`,
                                '</button>',
                            '</div>',
                        '</div>',
                    '</nav>',
                '</div>',
            '</div>',
        '</div>',
    ].join('\n')
    $('#rekruitmen-box').append(rekruitmenCard).append('<br>');
}
