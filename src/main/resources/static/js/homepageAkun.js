$(document).ready(() => {
    let url;
    if($(location).attr('href').includes("localhost")) {
        url = "http://localhost:8080";
    } else {
        url = "http://api-rekruit.herokuapp.com";
    }
	let urlFe;
    if($(location).attr('href').includes("localhost")) {
        urlFe = "http://localhost:8081";
    } else {
        urlFe = "http://rekruit-b15.herokuapp.com";
    }
    var idRekruitmen = $("#idRekruitmen").text();
    const tokenRekruiter = window.localStorage.getItem('token');
    $.ajax({
        type: "GET",
        url: url + "/pengumuman/rekruitmen/" + idRekruitmen,
        dataType: 'json',
        contentType: 'application/json',
        headers: {
         "Access-Control-Allow-Origin": "*",
         "Authorization": "Bearer " + tokenRekruiter,
        },
        success: function(data) {
            console.log(data);
            $("#dataPengumuman").empty();
            for (var i = 0 ; i < data.length ; i++) {//cek data.

                var lastEdit = new Date(JSON.parse(JSON.stringify(data[i].waktu)));

                var content = '<div class="card" style="margin-top: 20px;">' +
                '<div class="card-content">' +
                    '<div class="content"><bold>' +
                    data[i].judul +
                      '</bold><div></div><label>Last Edit: </label>' +
                      '<p>' + lastEdit +'</p>' +
                      '<p>'+data[i].isi+'</p>' +
                    '</div> </div>' +
                    '<div class="card">' +
                    '<footer class="card-footer">' +
                      '<button id="'+ data[i].id + '"onClick="deleteButton(this.id)" class="card-footer-item button is-danger deleteButton">Hapus</button>' +
                      '<a href="'+ urlFe+''+'/pengumuman/ubah/'+data[i].rekruitmenModel.id+'/'+data[i].id+'" class="card-footer-item button is-primary">Ubah Pengumuman</a>' +
                    '</footer>' +
                '</div></div>'

                $("#dataPengumuman").append(content);
            }
        }
    });    
});

function deleteButton(idRekruitmen) {
    let url;
    if($(location).attr('href').includes("localhost")) {
        url = "http://localhost:8080";
    } else {
        url = "http://api-rekruit.herokuapp.com";
    } 
    
    let redirectUrl;
    if($(location).attr('href').includes("localhost")) {
        redirectUrl = "http://localhost:8081";
    } else {
        redirectUrl = "http://rekruit-b15.herokuapp.com";
    } 
    
    var tokenRekruiter = window.localStorage.getItem('token');
    var link = url + "/pengumuman/"+idRekruitmen;
    $.ajax({
      type: 'DELETE',
      url: link,
      dataType: 'json',
      contentType: 'application/json',
      headers: {
       "Access-Control-Allow-Origin": "*",
      "Authorization": "Bearer " + tokenRekruiter,
      },
      success: function (data) {
          alert("Berhasil dihapus")
          window.location = redirectUrl + "/pengumuman/"+idRekruitmen;
      }
    });
}


