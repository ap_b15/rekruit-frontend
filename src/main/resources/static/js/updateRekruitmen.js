$("#buatRekruitmen").click(() => {
    $("#buatRekruitmen").addClass("is-loading");

    let url;
    if($(location).attr('href').includes("localhost")) {
        url = "http://localhost:8080";
    } else {
        url = "http://api-rekruit.herokuapp.com";
    }

    let urlFe;
    if($(location).attr('href').includes("localhost")) {
        urlFe = "http://localhost:8081";
    } else {
        urlFe = "http://rekruit-b15.herokuapp.com/";
    }

    var idRekruitmen = $("#idRekruitmen").text(); // Mock id for creating rekruitmen
    let tokenRekruiter = window.localStorage.getItem('token');;

    const rekruitmen = {
        //    email:$("#email").val(),
        judul:$("#judul").val(),
        narahubung:$("#narahubung").val(),
        deskripsiTugas:$("#deskripsiTugas").val(),
        deskripsiPekerjaan:$("#deskripsiPekerjaan").val(),
        syaratKetentuan:$("#syaratKetentuan").val(),
        startDateRegistrasi:$("#startDateRegistrasi").val(),
        dueDateRegistrasi:$("#dueDateRegistrasi").val(),
        dueDateTugas:$("#dueDateTugas").val(),
        linkWawancara:$("#linkWawancara").val(),
    };
    console.log(rekruitmen);

    $.ajax({
        type: "PUT",
        url: url + '/rekruter/' + idRekruitmen,
        dataType: 'json',
        contentType: 'application/json',
        headers: {
            "Access-Control-Allow-Origin": "*",
            "Authorization": "Bearer " + tokenRekruiter,
        },
        success: function (data) {
            alert("BERHASIL UPDATE REKRUITMEN");
            window.location.href = urlFe + '/rekruter/dashboard';
        },
        data: JSON.stringify(rekruitmen)
    })
});