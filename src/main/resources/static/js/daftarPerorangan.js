$(".signup-button").click(() => {
    $(".signup-button").addClass("is-loading");
    let url;
    if($(location).attr('href').includes("localhost")) {
        url = "http://localhost:8080";
    } else {
        url = "http://api-rekruit.herokuapp.com";
    }

    let redirectUrl;
    if($(location).attr('href').includes("localhost")) {
        redirectUrl = "http://localhost:8081";
    } else {
        redirectUrl = "http://rekruit-b15.herokuapp.com/";
    } 
    const person = {
        email: $("#email").val(),
        password: $("#password").val(),
        userRole:"PENDAFTAR",
        namaLengkap:$("#namaLengkap").val(),
        npm: $("#npm").val(),
        fakultas: $("#fakultas").val(),
        kontak: $("#kontak").val()
    }

    $.ajax({
        type: 'post',
        url: url + "/auth/register/pendaftar",
        dataType: 'json',
        contentType:'application/json',
        success: () => {
            alert("Berhasil mendaftar sebagai Pendaftar");
            window.location = redirectUrl+ "/login";
        },
        error: () => {
            alert("Kamu telah terdaftar");
            $(".signup-button").removeClass("is-loading");
        },
        data: JSON.stringify(person)
    });
});