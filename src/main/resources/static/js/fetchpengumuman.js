$(document).ready(() => {
    let tokenRekruiter = window.localStorage.getItem('token');
    let id  = $("#id").text();
    let url;
    if($(location).attr('href').includes("localhost")) {
        url = "http://localhost:8080";
    } else {
        url = "http://api-rekruit.herokuapp.com";
    }

    let urlFe;
    if($(location).attr('href').includes("localhost")) {
        urlFe = "http://localhost:8081";
    } else {
        urlFe = "http://rekruit-b15.herokuapp.com";
    }

    $.ajax({
        type: "GET",
        url: url + `/pengumuman/` + id,
        dataType: 'json',
        contentType: 'application/json',
        headers: {
            "Access-Control-Allow-Origin": "*",
            "Authorization": "Bearer " + tokenRekruiter,
        },
        success: function(data) {
            //alert('BERHASIL GET REKRUITMEN');
            //console.log(data); // DEBUG

            if ($(location).attr('href').includes("ubah")) {
                    $("#judul").val(data.judul);
                    $("#isiPengumuman").val(data.isi);
            }
        }
    });
});