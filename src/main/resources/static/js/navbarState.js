$(document).ready(() => {
    // Check for click events on the navbar burger icon
    $(".navbar-burger").click(() => {
        // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
        $(".navbar-burger").toggleClass("is-active");
        $(".navbar-menu").toggleClass("is-active");
    });

    const authenticatedUser = localStorage.getItem("email");
    const userRole = localStorage.getItem("role");

    if(authenticatedUser != null) {
        let dashBoard;

        if(userRole == "REKRUTER") {
            dashBoard = `<a href="/rekruter/dashboard" class="navbar-item"><strong>Dashboard</strong> </a>`
        } else {
            dashBoard = `<a href="/pendaftar/dashboard" class="navbar-item"><strong>Dashboard</strong> </a>`
        }

        if(userRole =="PENDAFTAR") {
            $(".navbar-end").append(`<a href="/pendaftar/pengumuman" class="navbar-item"><strong>Pengumuman</strong></a>`)
        }
        // Change the button group of Sign Up and Login into logged user email
        $(".auth-state").remove()
        $(".navbar-end")
            .append(dashBoard)
            .append(`<a class="navbar-item"><strong>${authenticatedUser}</strong></a>`)
            .append(`<a id="logout" class="navbar-item"><strong>Log Out</strong></a>`)

        // Only for landing page
        $("#auth-button").remove()
    }

    $("#logout").click(() => {
    localStorage.clear();
    window.location = "/"
    })

});

