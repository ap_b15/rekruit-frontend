$(document).ready(() => {
    let url;
    if($(location).attr('href').includes("localhost")) {
        url = "http://localhost:8080";
    } else {
        url = "http://api-rekruit.herokuapp.com";
    }

    let redirectUrl;
    if($(location).attr('href').includes("localhost")) {
        redirectUrl = "http://localhost:8081";
    } else {
        redirectUrl = "http://rekruit-b15.herokuapp.com/";
    }

    const idPendaftar = window.localStorage.getItem('id');
    const tokenRekruiter = window.localStorage.getItem('token');

    $.ajax({
        type: "GET",
        url: url + "/pendaftar/" + idPendaftar,
        dataType: 'json',
        contentType: 'application/json',
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Authorization": "Bearer " + tokenRekruiter,
        },
        success: function (data) {
            for (var i = 0 ; i < data.mendaftarRekruitmen.length ; i++) {
                
                pengumumanAccessor(data.mendaftarRekruitmen[i].judul,data.mendaftarRekruitmen[i].id)
            }  
        }
    });
});

function pengumumanAccessor(judul,id) {
    let url;
    if($(location).attr('href').includes("localhost")) {
        url = "http://localhost:8080";
    } else {
        url = "http://api-rekruit.herokuapp.com";
    }

    let redirectUrl;
    if($(location).attr('href').includes("localhost")) {
        redirectUrl = "http://localhost:8081";
    } else {
        redirectUrl = "http://rekruit-b15.herokuapp.com/";
    }

    const idPendaftar = window.localStorage.getItem('id');
    const tokenRekruiter = window.localStorage.getItem('token');

    $.ajax({
        type: "GET",
        url: url + "/pengumuman/rekruitmen/" + id,
        dataType: 'json',
        contentType: 'application/json',
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Authorization": "Bearer " + tokenRekruiter,
        },
        success: function (data) {
            console.log(data);
            console.log("TEST");
            console.log(data.length);
            $("#dataDashboard").append('<h4 class="title is-4" style="margin-top:10px">'+ judul +'</h4>')
            // console.log("INI " + data.0.judul);
            for (var i = 0 ; i < data.length ; i++) {
                var waktu =  new Date(JSON.parse(JSON.stringify(data[i].waktu)));
                // console.log(data[i].judul);
                var content = '<div class="card" style = "margin-top:5px">' +
                '<div class="card-content">' +
                  '<div class="content">' +
                    '<p style="font-size:20px"><strong>'+data[i].judul+'</strong></p>' +
                    '<p>' + waktu +'</p>' +
                    '<p>' + data[i].isi + '</p>' +
                  '</div>'+
                '</div>' +
              '</div>' 

              $("#dataDashboard").append(content);

            }

        }
    });
}