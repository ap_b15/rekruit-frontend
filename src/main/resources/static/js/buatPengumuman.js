$("#buatPengumuman").click(() => {
    $("#buatPengumuman").addClass("is-loading");
    let url;
    if($(location).attr('href').includes("localhost")) {
        url = "http://localhost:8080";
    } else {
        url = "http://api-rekruit.herokuapp.com";
    }

    let urlFe;
    if($(location).attr('href').includes("localhost")) {
        urlFe = "http://localhost:8081";
    } else {
        urlFe = "http://rekruit-b15.herokuapp.com";
    }
    var idRekruitmen = $("#idRekruitmen").text(); 
    let tokenRekruiter = window.localStorage.getItem('token');

    const pengumuman = {
        judul:$("#judul").val(),
        isi:$("#isiPengumuman").val(),
        
    };
    console.log(pengumuman);

    $.ajax({
        type: 'POST',
        url: url + '/rekruter/pengumuman/' + idRekruitmen,
        dataType: 'json',
        contentType: 'application/json',
        headers: {
           "Access-Control-Allow-Origin": "*",
           "Authorization": "Bearer " + tokenRekruiter,
        },
        success: function (data) {
            alert("BERHASIL MEMBUAT PENGUMUMAN");
            console.log(data);
            window.location.href = urlFe + '/pengumuman/' + idRekruitmen;
        },
        data: JSON.stringify(pengumuman)
    })
});
