$(document).ready(() => {
    let url;
    if($(location).attr('href').includes("localhost")) {
        url = "http://localhost:8080";
    } else {
        url = "http://api-rekruit.herokuapp.com";
    }

    const tokenRekruiter = window.localStorage.getItem('token');

    $.ajax({
        type: "GET",
        url: url + "/rekruter/rekruitmen/" + idRekruitmen,
        dataType: 'json',
        contentType: 'application/json',
        success: (data) => {
            $("#detailRekruitmen").empty();
            let dueDateRegistrasi = new Date(data.dueDateRegistrasi);
            let content = '<p class="is-size-1" >' + data.judul +'</p >' +
            '<p class="is-size-5">Due Date Registrasi: ' + dueDateRegistrasi.toDateString() +'</p>' +
            '<div class="card">' +
                '<div class="card-content">'+
                    '<div class="content">' +
                        '<label><strong>Deskripsi Kegiatan:</strong></label>' +
                        '<p>' +data.deskripsiPekerjaan+'</p>' +
                        '<label><strong>Syarat dan Ketentuan:</strong></label>' +
                        '<p>'+data.syaratKetentuan+'</p>' +
                        '<label><strong>Tugas:</strong></label>' +
                        '<p>'+data.deskripsiTugas+'</p>' +
                        '<label><strong>Narahubung: </strong></label>' +
                        '<p>' + data.narahubung + '</p>' +
                        '<button class="button is-primary buttonDaftar">Daftar</button>' +
                    '</div>' +
                '</div>' +
              '</div>'
            $("#detailRekruitmen").append(content);
        }
    });
});

$(document).on('click',".buttonDaftar", () => {
    let url;
    if($(location).attr('href').includes("localhost")) {
        url = "http://localhost:8080";
    } else {
        url = "http://api-rekruit.herokuapp.com";
    }

    let redirectUrl;
    if($(location).attr('href').includes("localhost")) {
        redirectUrl = "http://localhost:8081";
    } else {
        redirectUrl = "http://rekruit-b15.herokuapp.com/";
    } 

    const idPendaftar = window.localStorage.getItem('id');

    const tokenRekruiter = window.localStorage.getItem('token');
    $.ajax({
        type: "POST",
        url: url + "/pendaftar/" + idPendaftar + '/' + idRekruitmen,
        dataType: 'json',
        contentType: 'application/json',
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Authorization": "Bearer " + tokenRekruiter,
        },
        success: () => {
            alert("Berhasil Mendaftar");
            window.location = redirectUrl + "/pendaftar/dashboard";
        }
    });
})