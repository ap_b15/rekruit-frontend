$("#redirectPengumuman").click(() => {
    let redirectUrl;
    if($(location).attr('href').includes("localhost")) {
        redirectUrl = "http://localhost:8081";
    } else {
        redirectUrl = "http://rekruit-b15.herokuapp.com";
    }

    window.location.replace(redirectUrl + '/pendaftar/pengumuman');
});