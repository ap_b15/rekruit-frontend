let cachedResponse;

$(document).ready(() => {
    let url;
    if($(location).attr('href').includes("localhost")) {
        url = "http://localhost:8080";
    } else {
        url = "http://api-rekruit.herokuapp.com";
    }

    $.ajax({
      type: "GET",
      url: url + "/rekruter/rekruitmen",
      dataType: 'json',
      contentType: 'applicatsion/json',
      success: (response) => {
          console.log(response)
          cachedResponse = response;
          showAll(cachedResponse);
      }
    });
});

$("#show-all").click(() => {
    $("#rekruitmen-box").empty();
    showAll(cachedResponse);
})

$("#show-belum-dibuka").click(() => {
    $("#rekruitmen-box").empty();
    showBelumDibuka(cachedResponse);
})

$("#show-dibuka").click(() => {
    $("#rekruitmen-box").empty();
    showSudahDibuka(cachedResponse);
})

$("#show-ditutup").click(() => {
    $("#rekruitmen-box").empty();
    showSudahDitutup(cachedResponse);
})

function showAll(response) {
    response.forEach((element) => {
        renderElement(element);
    })
}

function showBelumDibuka(response) {
    console.log("click")
    response.forEach((element) => {
        if(element.status === 'Belum Dibuka') {
            renderElement(element);
        }
    })
}

function showSudahDibuka(response) {
    response.forEach((element) => {
        if(element.status === 'Sudah Dibuka') {
            renderElement(element);
        }
    })
}

function showSudahDitutup(response) {
    response.forEach((element) => {
        if(element.status === 'Sudah Ditutup') {
            renderElement(element);
        }
    })
}

function renderElement(element) {
    let id = element.id;
    let namaRekruter = element.rekruiter.namaRekruter;
    let judul = element.judul;
    let startDateRegis = new Date(element.startDateRegistrasi).toDateString();
    let dueDateRegis = new Date(element.dueDateRegistrasi).toDateString();
    let status = element.status;
    let daftarButton;
    let statusText;

    if(status === "Sudah Dibuka") {
        daftarButton = `<button class="button is-primary buttonDaftar" onclick="daftarRekrutmen(${id})">Daftar</button>`;
        statusText = `<p class="subtitle is-5"><strong class="has-text-success">Dibuka</strong></p>`;
    } else {
        daftarButton = `<button disabled class="button is-primary buttonDaftar" onclick="daftarRekrutmen(${id})">Daftar</button>`;
        if(status === "Belum Dibuka") {
            statusText = `<p class="subtitle is-5"><strong class="has-text-warning">Belum Dibuka</strong></p>`;
        } else {
            statusText = `<p class="subtitle is-5"><strong class="has-text-danger">Ditutup</strong></p>`;
        }
    }

    let rekruitmenCard = [
        '<div class="column is-one-third">',
        '<div class="card is-centered">',
        '<div class="card-content has-text-centered">',
        `<p class="title is-5">${namaRekruter}</p>`,
        `<p class="subtitle is-4"><strong>${judul}</strong></p>`,
        '<br>',
        '<p class="title is-6">Pembukaan Registrasi</p>',
        `<p class="subtitle is-5"><strong>${startDateRegis}</strong></p>`,
        '<p class="title is-6">Status</p>',
        statusText,
        '<p class="title is-6">Due Date Registrasi</p>',
        `<p class="subtitle is-5"><strong>${dueDateRegis}</strong></p>`,
        '<div id="rekruitmen-button-group" class="buttons is-centered">',
        `<a href="/rekruitmen/detail/${id}"><button class="button">Detil</button></a>`,
        daftarButton,
        '</div>',
        '</div>',
        '</div>',
        '</div>'
    ].join('\n');
    $("#rekruitmen-box").append(rekruitmenCard);
}

function daftarRekrutmen(idRekruitmen) {
    let url;
    if($(location).attr('href').includes("localhost")) {
        url = "http://localhost:8080";
    } else {
        url = "http://api-rekruit.herokuapp.com";
    }

    let redirectUrl;
    if($(location).attr('href').includes("localhost")) {
        redirectUrl = "http://localhost:8081";
    } else {
        redirectUrl = "http://rekruit-b15.herokuapp.com/";
    }

    const idPendaftar = window.localStorage.getItem('id');

    const tokenRekruiter = window.localStorage.getItem('token');
    $.ajax({
        type: "POST",
        url: url + "/pendaftar/" + idPendaftar + '/' + idRekruitmen,
        dataType: 'json',
        contentType: 'application/json',
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Authorization": "Bearer " + tokenRekruiter,
        },
        success: () => {
            alert("Berhasil Mendaftar");
            window.location = redirectUrl + "/pendaftar/dashboard";
        }
    });
};
