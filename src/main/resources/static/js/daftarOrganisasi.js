$(".signup-button").click(() => {
    $(".signup-button").addClass("is-loading");
    let url;
    if($(location).attr('href').includes("localhost")) {
        url = "http://localhost:8080";
    } else {
        url = "http://api-rekruit.herokuapp.com";
    }

    let redirectUrl;
    if($(location).attr('href').includes("localhost")) {
        redirectUrl = "http://localhost:8081";
    } else {
        redirectUrl = "http://rekruit-b15.herokuapp.com/";
    } 

    const rekruter = {
         email : $("#email").val() ,
         password : $("#password").val() ,
         userRole : "REKRUTER" ,
         namaRekruter : $("#namaRekruter").val() ,
         deskripsiRekruter : $("#namaRekruter").val()
    }

    $.ajax({
        type: 'post',
        url: url + "/auth/register/rekruter",
        dataType: 'json',
        contentType:'application/json',
        success: () => {
            alert("Berhasil mendaftar sebagai Rekruter");
            window.location = redirectUrl+ "/login"
        },
        error: () => {
            alert("Organisasi/Kepanitiaanmu telah terdaftar");
             $(".signup-button").removeClass("is-loading");
        },
        data: JSON.stringify(rekruter)
    });
});