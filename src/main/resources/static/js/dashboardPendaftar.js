let url;
if($(location).attr('href').includes("localhost")) {
    url = "http://localhost:8080";
} else {
    url = "http://api-rekruit.herokuapp.com";
}

let redirectUrl;
if($(location).attr('href').includes("localhost")) {
    redirectUrl = "http://localhost:8081";
} else {
    redirectUrl = "http://rekruit-b15.herokuapp.com";
}

const idPendaftar = window.localStorage.getItem('id');
const tokenRekruiter = window.localStorage.getItem('token');
const linksubmit = redirectUrl  + "/pendaftar/submisi/";

$(document).ready(() => {

    $.ajax({
        type: "GET",
        url: url + "/pendaftar/" + idPendaftar,
        dataType: 'json',
        contentType: 'application/json',
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Authorization": "Bearer " + tokenRekruiter,
        },
        success: function(data) {
            $("#dataDashboard").empty();
            for (let i = 0 ; i < data.mendaftarRekruitmen.length ; i++) {

                let duedate = new Date(JSON.parse(JSON.stringify(data.mendaftarRekruitmen[i].dueDateTugas)));

                let content = '<div class="card" style="margin-top: 20px;">' +
                '<header class="card-header"> <p class="card-header-title">' +
                    data.mendaftarRekruitmen[i].judul +
                '</p></header>' +
                '<div class="card-content">' +
                    '<div class="content">' +
                      '<label>Due Date Tugas: </label>' +
                      '<p>' + duedate +'</p>' +
                      '<label>Status: </label>' +
                      '<p>'+ panggilStatusRekruitmen(data.id, data.mendaftarRekruitmen[i].id) + '</p>' +
                      '<label>Tugas:</label>' +
                      '<p>' + data.mendaftarRekruitmen[i].deskripsiTugas + '</p>' +
                    '</div> </div>' +
                    '<div class="card">' +
                    '<footer class="card-footer">' +
                      '<button id="'+ data.mendaftarRekruitmen[i].id + '"onClick="deleteButton(this.id)" class="card-footer-item button is-danger deleteButton">Delete</button>' +
                      '<a href="' + linksubmit + data.mendaftarRekruitmen[i].id + '" class="card-footer-item button is-primary">Submisi Tugas</a>' +
                      '<a href="' + data.mendaftarRekruitmen[i].linkWawancara+ '" class="card-footer-item button is-link">Pesan Wawancara</a>' +
                    '</footer>' +
                '</div></div>'

                $("#dataDashboard").append(content);
            }
        }
    });    
});

$("#sortBy").change(function (e) {
    var e = document.getElementById("sortBy");
    let kode = e.value;
    let link = url + "/pendaftar/sort/"+idPendaftar+"/"+kode;
    $.ajax({
        type: "POST",
        url: link,
        dataType: 'json',
        contentType: 'application/json',
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Authorization": "Bearer " + tokenRekruiter,
        },
        success: function(data) {
            $("#dataDashboard").empty();
            for (let i = 0 ; i < data.mendaftarRekruitmen.length ; i++) {

                let duedate = new Date(JSON.parse(JSON.stringify(data.mendaftarRekruitmen[i].dueDateTugas)));

                let content = '<div class="card" style="margin-top: 20px;">' +
                '<header class="card-header"> <p class="card-header-title">' +
                    data.mendaftarRekruitmen[i].judul +
                '</p></header>' +
                '<div class="card-content">' +
                    '<div class="content">' +
                      '<label>Due Date Tugas: </label>' +
                      '<p>' + duedate +'</p>' +
                      '<label>Status: </label>' +
                      '<p>'+ panggilStatusRekruitmen(data.id, data.mendaftarRekruitmen[i].id) +'</p>' +
                      '<label>Tugas:</label>' +
                      '<p>' + data.mendaftarRekruitmen[i].deskripsiTugas + '</p>' +
                    '</div> </div>' +
                    '<div class="card">' +
                    '<footer class="card-footer">' +
                    '<button id="'+ data.mendaftarRekruitmen[i].id + '"onClick="deleteButton(this.id)" class="card-footer-item button is-danger deleteButton">Delete</button>'  +
                    '<a href="' + linksubmit + data.mendaftarRekruitmen[i].id + ' " class="card-footer-item button is-primary">Submisi Tugas</a>' +
                    '<a href="' + data.mendaftarRekruitmen[i].linkWawancara+ '" class="card-footer-item button is-link">Pesan Wawancara</a>' +
                    '</footer>' +
                '</div></div>'

                $("#dataDashboard").append(content);
            }
        }
    });
});

function deleteButton(idRekruitmen) {
    let idPendaftar = window.localStorage.getItem('id');
    let tokenRekruiter = window.localStorage.getItem('token');
    let link = url + "/pendaftar/"+idPendaftar+"/"+idRekruitmen;
    $.ajax({
      type: 'DELETE',
      url: link,
      dataType: 'json',
      contentType: 'application/json',
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Authorization": "Bearer " + tokenRekruiter,
      },
      success: () => {
          alert("Berhasil dihapus")
          window.location = redirectUrl + "/pendaftar/dashboard";
      }
    });
}

function panggilStatusRekruitmen(idPendaftar, idRekruitmen){
    let result = "";

     $.ajax({
            type: "GET",
            url: url + "/pendaftar/mendaftar/" + idPendaftar + "/" + idRekruitmen,
            dataType: 'json',
            contentType: 'application/json',
            async: false,
            headers: {
              "Access-Control-Allow-Origin": "*",
              "Authorization": "Bearer " + tokenRekruiter,
            },
            success: function(data) {
                result = (data.statusPenerimaan) != null ? data.statusPenerimaan : "Belum Dinilai";
            }
        });

     return result

}
