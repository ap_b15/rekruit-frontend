$(document).ready(() => {
    let tokenRekruiter = window.localStorage.getItem('token');
    let idRekruitmen = $("#idRekruitmen").text();
    let url;
    if($(location).attr('href').includes("localhost")) {
        url = "http://localhost:8080";
    } else {
        url = "http://api-rekruit.herokuapp.com";
    }

    let urlFe;
    if($(location).attr('href').includes("localhost")) {
        urlFe = "http://localhost:8081";
    } else {
        urlFe = "http://rekruit-b15.herokuapp.com/";
    }

    $.ajax({
        type: "GET",
        url: url + `/rekruter/rekruitmen/` + idRekruitmen,
        dataType: 'json',
        contentType: 'application/json',
        headers: {
            "Access-Control-Allow-Origin": "*",
            "Authorization": "Bearer " + tokenRekruiter,
        },
        success: function(data) {
            // alert('BERHASIL GET REKRUITMEN');
            console.log(data); // DEBUG

            if ($(location).attr('href').includes("ubah")) {
                    $("#judul").val(data.judul);
                    $("#narahubung").val(data.narahubung);
                    $("#deskripsiTugas").val(data.deskripsiTugas);
                    $("#deskripsiPekerjaan").val(data.deskripsiPekerjaan);
                    $("#syaratKetentuan").val(data.syaratKetentuan);
                    $("#startDateRegistrasi").val(data.startDateRegistrasi.slice(0, 10));
                    $("#dueDateRegistrasi").val(data.dueDateRegistrasi.slice(0, 10));
                    $("#dueDateTugas").val(data.dueDateTugas.slice(0, 10));
                    $("#linkWawancara").val(data.linkWawancara);
            }
        }
    });
});