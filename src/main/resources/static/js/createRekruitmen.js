$("#buatRekruitmen").click(() => {
    $("#buatRekruitmen").addClass("is-loading");

    let url;
    if($(location).attr('href').includes("localhost")) {
        url = "http://localhost:8080";
    } else {
        url = "http://api-rekruit.herokuapp.com";
    }

    let urlFe;
    if($(location).attr('href').includes("localhost")) {
        urlFe = "http://localhost:8081";
    } else {
        urlFe = "http://rekruit-b15.herokuapp.com/";
    }

    let idRekruter = localStorage.getItem("id"); // id for creating rekruitmen
    let tokenRekruiter = window.localStorage.getItem('token');
    let namaRekruter;

    $.ajax({
        type: 'GET',
        url: url + '/rekruter/get/' + idRekruter,
        dataType: 'json',
        contentType: 'application/json',
        headers: {
            "Access-Control-Allow-Origin": "*",
            "Authorization": "Bearer " + tokenRekruiter,
        },
        success: (response) => {
            namaRekruter = response.namaRekruter;
        },
    })

    const rekruitmen = {
        namaRekruiter: namaRekruter,
        judul:$("#judul").val(),
        narahubung:$("#narahubung").val(),
        deskripsiTugas:$("#deskripsiTugas").val(),
        deskripsiPekerjaan:$("#deskripsiPekerjaan").val(),
        syaratKetentuan:$("#syaratKetentuan").val(),
        startDateRegistrasi:$("#startDateRegistrasi").val(),
        dueDateRegistrasi:$("#dueDateRegistrasi").val(),
        dueDateTugas:$("#dueDateTugas").val(),
        linkWawancara:$("#linkWawancara").val(),
    };
    console.log(rekruitmen);

    $.ajax({
        type: 'POST',
        url: url + '/rekruter/' + idRekruter,
        dataType: 'json',
        contentType: 'application/json',
        headers: {
            "Access-Control-Allow-Origin": "*",
            "Authorization": "Bearer " + tokenRekruiter,
        },
        success: function (data) {
            alert("BERHASIL ADD REKRUITMEN");
            window.location.href = urlFe + '/rekruter/dashboard';
        },
        data: JSON.stringify(rekruitmen)
    })
});