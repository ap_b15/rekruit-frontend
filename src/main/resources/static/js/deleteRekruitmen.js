$("#deleteButton").click(() => {
    console.log("kepencet ni tombol deletenya");

    let tokenRekruiter = window.localStorage.getItem('token');
    let idRekruitmen = $("#idRekruitmen").text();
    let idRekruiter = window.localStorage.getItem('id');
    let url;
    if($(location).attr('href').includes("localhost")) {
        url = "http://localhost:8080";
    } else {
        url = "http://api-rekruit.herokuapp.com";
    }

    let urlFe;
    if($(location).attr('href').includes("localhost")) {
        urlFe = "http://localhost:8081";
    } else {
        urlFe = "http://rekruit-b15.herokuapp.com/";
    }

    $.ajax({
        type: "DELETE",
        url: url + `/rekruter/rekruitmen/` + idRekruiter + '/' + idRekruitmen,
        dataType: 'json',
        contentType: 'application/json',
        headers: {
            "Access-Control-Allow-Origin": "*",
            "Authorization": "Bearer " + tokenRekruiter,
        },
        success: function() {
            console.log(('BERHASIL HAPUS REKRUITMEN'));
            alert('BERHASIL HAPUS REKRUITMEN');
            window.location.href = urlFe + '/rekruter/dashboard';
        }
    });

    window.location.href = urlFe + '/rekruter/dashboard';
});