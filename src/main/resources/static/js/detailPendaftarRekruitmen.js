$(document).ready(() => {
    let url;
    if($(location).attr('href').includes("localhost")) {
        url = "http://localhost:8080";
    } else {
        url = "http://api-rekruit.herokuapp.com";
    }
    let urlFe;
    if($(location).attr('href').includes("localhost")) {
        urlFe = "http://localhost:8081";
    } else {
        urlFe = "http://rekruit-b15.herokuapp.com/";
    }

    const tokenRekruiter = window.localStorage.getItem('token');
    const idPendaftar = window.localStorage.getItem('id');

    $.ajax({
        type: "GET",
        url: url + "/rekruter/rekruitmen/" + idRekruitmen,
        dataType: 'json',
        contentType: 'application/json',
        headers: {
                  "Access-Control-Allow-Origin": "*",
                  "Authorization": "Bearer " + tokenRekruiter,
                },
        success: (data) => {
            $("#detailPendaftarRekruitmen").empty();
            let startDateRegistrasi = new Date(data.startDateRegistrasi);
            let dueDateRegistrasi = new Date(data.dueDateRegistrasi);
            let content = '<p class="is-size-3" >'+ data.judul +'</p >' +
            '<br><br>' +
            '<p class="is-size-4" ><b>'+ data.rekruiter.namaRekruter +'</b></p >' +
            '<p class="is-size-5"> Timeline: From '+ startDateRegistrasi.toDateString()+ ' to ' +dueDateRegistrasi.toDateString()+ '</p >' +
            '<br>' +
            '<div class="card">' +
                '<div class="card-content">'+
                    '<div class="content">' +
                        '<p class="is-size-4" > <b>Deskripsi Pekerjaan</b></p >' +
                        '<p class="is-size-5">' + data.deskripsiPekerjaan +'</p>' +
                        '<p class="is-size-4" > <b>Syarat dan Ketentuan</b></p >' +
                        '<p class="is-size-5">' + data.syaratKetentuan +'</p>' +
                        '<p class="is-size-4" > <b>Tugas</b></p>' +
                        '<p class="is-size-5">' + data.deskripsiTugas +'</p>' +
                    '</div>' +
                '</div>' +
              '</div>' +
              '<br><br>' +
              '<br>'
            $("#detailPendaftarRekruitmen").append(content);
        }
    });

    $.ajax({
            type: "GET",
            url: url + "/rekruter/mendaftar/" + idRekruitmen,
            dataType: 'json',
            contentType: 'application/json',
            headers: {
                      "Access-Control-Allow-Origin": "*",
                      "Authorization": "Bearer " + tokenRekruiter,
                    },
            success: (data) => {
                $("#tablePendaftarRekruitmen").empty();
                let dataPendaftar = '';
                var index;
                for(i = 0; i < data.length; i++){
                    let submitted = (data[i].linkTugas === null || data[i].linkTugas === "") ? "Not Submitted Yet" : "Submitted"
                    let nilai = data[i].nilai === -1 ? "Belum Dinilai" : data[i].nilai;
                    let status = data[i].statusPenerimaan == null ? "Belum Dinilai" : data[i].statusPenerimaan;
                    dataPendaftar = dataPendaftar +
                                    '<tr>' +
                                        '<td>'+ data[i].pendaftarModel.npm+'</th>' +
                                        '<td>'+ data[i].pendaftarModel.namaLengkap+'</th>' +
                                        '<td>'+ data[i].pendaftarModel.fakultas+'</th>' +
                                        '<td><a href="'+ urlFe + '/rekruitmen/detail-data-pendaftar-rekruitmen/' +
                        data[i].pendaftarModel.id + '/' + idRekruitmen +'">Detail</a></th>' +
                                        '<td>'+ submitted +'</th>' +
                                        '<td>'+ nilai +'</th>' +
                                        '<th>'+ status + '</th>' +
                                    '</tr>'
                }
                let content = '<p class="is-size-3" >List Pendaftar</p >' +
                '<div class="card">' +
                    '<div class="card-content">'+
                        '<div class="content">' +
                            '<table class="table">' +
                                '<thead>' +
                                    '<tr>' +
                                        '<th>NPM</th>' +
                                        '<th>Nama</th>' +
                                        '<th>Fakultas</th>' +
                                        '<th>Tugas</th>' +
                                        '<th>Status Tugas</th>' +
                                        '<th>Nilai</th>' +
                                        '<th>Status</th>' +
                                    '</tr>' +
                                '</thead>' +
                                '<tbody>' +
                                    dataPendaftar +
                                '</tbody>' +
                            '</table>' +
                        '</div>' +
                    '</div>' +
                  '</div>'
                $("#tablePendaftarRekruitmen").append(content);
            }
        });

});