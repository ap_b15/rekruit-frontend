let url;
if($(location).attr('href').includes("localhost")) {
    url = "http://localhost:8080";
} else {
    url = "http://api-rekruit.herokuapp.com";
}

let redirectUrl;
if($(location).attr('href').includes("localhost")) {
    redirectUrl = "http://localhost:8081";
} else {
    redirectUrl = "http://rekruit-b15.herokuapp.com";
}
const tokenRekruiter = window.localStorage.getItem('token');
const idPendaftar = window.localStorage.getItem('id');

$(document).ready(() => {
    $.ajax({
        type: "GET",
        url: url + "/pendaftar/mendaftar/" + idPendaftar + "/" + idRekruitmen,
        dataType: 'json',
        contentType: 'application/json',
        headers: {
                  "Access-Control-Allow-Origin": "*",
                  "Authorization": "Bearer " + tokenRekruiter,
        },
        success: (data) => {
            $("#submisiData").empty();
            let dueDateTugas = new Date(data.rekruitmenModel.dueDateTugas);
            let linkCV = (data.linkCV === null ? '' : data.linkCV);
            let urlCV = linkCV.includes("http") ? linkCV : "http://" + linkCV;
            let isiLinkCV = urlCV != "http://" ? `<p><a href="${urlCV}">${urlCV}</a></p>` : `<p> </p>`
            let linkTugas = (data.linkTugas === null ? '' : data.linkTugas);
            let urlTugas = linkTugas.includes("http") ? linkTugas : "http://" + linkTugas;
            let isiLinkTugas = urlTugas != "http://" ? `<p><a href="${urlTugas}">${urlTugas}</a></p>` : `<p> </p>`
            let content = '<p class="is-size-1" >Submission</p >' +
            '<div class="card">' +
                '<div class="card-content">'+
                    '<div class="content">' +
                        '<p class="is-size-5" > <b>' + data.rekruitmenModel.judul +'</b></p >' +
                        '<p class="is-size-5">Due Date Pengumpulan Tugas: ' + dueDateTugas.toDateString() +'</p>' +
                          '<div class="field">' +
                            '<label><strong>Link CV:</strong></label>' +
                            isiLinkCV +
                            '<div class="control">' +
                              '<input id="linkCV" class="input" type="text" placeholder="masukkan link cv anda disini" required>' +
                            '</div>' +
                          '</div>' +
                          '<div class="field">' +
                             '<label><strong>Tugas:</strong></label>' +
                             isiLinkTugas +
                             '<div class="control">' +
                               '<input id="linkTugas" class="input" type="text" placeholder="masukkan link submisi tugas disini" required>' +
                             '</div>' +
                           '</div>' +
                            '<button class="button is-primary buttonSubmit">Submit</button>'+
                    '</div>' +
                '</div>' +
              '</div>'
            $("#submisiData").append(content);
        }
    });
});

$(document).on('click',".buttonSubmit", () => {
    const dataMendaftar = {
      linkCV: $("#linkCV").val(),
      linkTugas: $("#linkTugas").val()
    }

    const idPendaftar = window.localStorage.getItem('id');
    const tokenRekruiter = window.localStorage.getItem('token');
    const idMendaftar = "" + idPendaftar + "-" + idRekruitmen;
    console.log(dataMendaftar);

    if($("#linkCV").val() != "" && $("#linkTugas").val() != "" ) {
      $.ajax({
          type: "PUT",
          url: url + "/pendaftar/" +"mendaftar/" + idMendaftar,
          dataType: 'json',
          contentType: 'application/json',
          headers: {
            "Access-Control-Allow-Origin": "*",
            "Authorization": "Bearer " + tokenRekruiter,
          },
          success: () => {
              alert("Berhasil di Submit");
              window.location = redirectUrl + "/pendaftar/dashboard";
          },
          data: JSON.stringify(dataMendaftar)
      });
    } else {
      alert("Data kosong");
    }
})
