$("#login-button").click(() => {
    $("#login-button").addClass("is-loading");

    let url;
    if($(location).attr('href').includes("localhost")) {
        url = "http://localhost:8080";
    } else {
        url = "http://api-rekruit.herokuapp.com";
    }

    let redirectUrl;
    if($(location).attr('href').includes("localhost")) {
        redirectUrl = "http://localhost:8081";
    } else {
        redirectUrl = "http://rekruit-b15.herokuapp.com/";
    } 

    const authData = {
        email: $("#email").val(),
        password: $("#password").val()
    }

    $.ajax({
        type: 'POST',
        url: url + "/auth",
        contentType:'application/json',
        data: JSON.stringify(authData),
        dataType: 'json',
        success: (response) => {
            let emailRes = response.email;
            let tokenRes = response.token;
            let roleRes = response.role;
            let idRes = response.id;
            // console.log(response)
            // Set email to localStorage
            localStorage.setItem("email", emailRes);
            // Set token to localStorage
            localStorage.setItem("token", "Bearer " + tokenRes);
            // Set role to localStorage
            localStorage.setItem("role", roleRes);
            // Set id to localStorage
            localStorage.setItem("id",idRes);
            
            alert("Authentication Successful");
            window.location = redirectUrl;
        },
        error: () => {
            alert("Bad Credentials");
            $("#login-button").removeClass("is-loading");
        }
    })
  });