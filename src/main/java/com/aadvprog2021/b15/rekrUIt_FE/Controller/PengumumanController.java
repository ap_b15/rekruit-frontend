package com.aadvprog2021.b15.rekrUIt_FE.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class PengumumanController {
    
    @RequestMapping("pengumuman/{idRekruitmen}")
    public String showPengumuman(Model model, @PathVariable(value = "idRekruitmen") long idRekruitmen) throws Exception {
        model.addAttribute("idRekruitmen", idRekruitmen);
        return "pengumuman/hompageAkun";
    }
    
    @RequestMapping("pengumuman/buat/{idRekruitmen}")
    public String tambahPengumuman(Model model, @PathVariable(value = "idRekruitmen") long idRekruitmen) throws Exception {
        model.addAttribute("idRekruitmen", idRekruitmen);
        return "pengumuman/buatPengumuman";
    }

    @RequestMapping("pengumuman/ubah/{idRekruitmen}/{id}")
    public String ubahPengumuman(Model model, @PathVariable(value = "idRekruitmen") long idRekruitmen, @PathVariable(value = "id") long id) throws Exception {
        model.addAttribute("idRekruitmen", idRekruitmen);
        model.addAttribute("idRekruitmen", idRekruitmen);
        return "pengumuman/ubahPengumuman";
    }
    
}
