package com.aadvprog2021.b15.rekrUIt_FE.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class RekruterController {
    
    @RequestMapping("rekruter/dashboard")
    public String dashboardRekruter() throws Exception {
        return "rekruter/dashboardRekruter";
    }
    
}
