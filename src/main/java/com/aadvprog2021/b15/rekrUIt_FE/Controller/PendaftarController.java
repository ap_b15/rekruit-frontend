package com.aadvprog2021.b15.rekrUIt_FE.Controller;

import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
@Controller
public class PendaftarController {
    
    @RequestMapping("rekruitmen/detail/{idRekruitmen}")
    public String showDetailRekruitmen(@PathVariable(value = "idRekruitmen") long idRekruitmen, Map<String, Object> model) throws Exception {
        model.put("idRekruitmen", idRekruitmen);
        return "pendaftar/rekruitmenDetail";
    }

    @RequestMapping("pendaftar/dashboard")
    public String dashboardPendaftar() throws Exception {
        return "pendaftar/dashboardPendaftar";
    }

    @RequestMapping("pendaftar/submisi/{idRekruitmen}")
    public String submisiPendaftar(@PathVariable(value = "idRekruitmen") long idRekruitmen, Map<String, Object> model) throws Exception {
        model.put("idRekruitmen", idRekruitmen);
        return "pendaftar/submisiPendaftar";
    }

    @RequestMapping("pendaftar/pengumuman")
    public String listPengumuman() throws  Exception {
        return "pendaftar/dashboardPengumuman";
    }
}
