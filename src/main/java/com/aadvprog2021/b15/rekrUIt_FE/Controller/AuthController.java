package com.aadvprog2021.b15.rekrUIt_FE.Controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class AuthController {

    @RequestMapping("login")
    public String login() throws Exception {
        return "auth/login";
    }

    @RequestMapping("signup")
    public String signup() throws Exception {
        return "auth/pilihRole";
    }

    @RequestMapping("signup/perorangan")
    public String signupPerorangan() throws  Exception {
        return "auth/daftarPerorangan";
    }

    @RequestMapping("signup/organisasi")
    public String signupOrganisasi() throws Exception {
        return "auth/daftarOrganisasi";
    }

}
