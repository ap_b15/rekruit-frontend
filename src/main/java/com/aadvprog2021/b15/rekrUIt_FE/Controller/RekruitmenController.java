package com.aadvprog2021.b15.rekrUIt_FE.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class RekruitmenController {
    
    @RequestMapping("rekruitmen")
    public String showRekrutmen() throws Exception {
        return "showRekruitmen";
    }
    
    @RequestMapping("rekruitmen/buat")
    public String tambahRekrutmen() throws Exception {
        return "rekruitmen/buatRekruitmen";
    }

    @RequestMapping("rekruitmen/ubah/{idRekruitmen}")
    public String ubahRekrutmen(Model model, @PathVariable(value = "idRekruitmen") long idRekruitmen) throws Exception {
        model.addAttribute("idRekruitmen", idRekruitmen);
        return "rekruitmen/ubahRekruitmen";
    }

    @RequestMapping("rekruitmen/detail-rekruitmen/{idRekruitmen}")
    public String detailPendaftarRekrutmen(Model model, @PathVariable(value = "idRekruitmen") long idRekruitmen) throws Exception {
        model.addAttribute("idRekruitmen", idRekruitmen);
        return "rekruitmen/detailPendaftarRekruitmen";
    }
    @RequestMapping("rekruitmen/detail-data-pendaftar-rekruitmen/{idPendaftar}/{idRekruitmen}")
    public String detailDataPendaftarRekrutmen(Model model, @PathVariable(value = "idRekruitmen") long idRekruitmen, @PathVariable(value = "idPendaftar") long idPendaftar) throws Exception {
        model.addAttribute("idRekruitmen", idRekruitmen);
        model.addAttribute("idPendaftar", idPendaftar);
        return "rekruitmen/detailDataPendaftarRekruitmen";
    }

    
}
